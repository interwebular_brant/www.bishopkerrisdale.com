(function( $ , window , document ){

    var init = function() {
        setHeight();
        //snapScroll();
    };

    $(window).resize(function(){
        setHeight();
        //snapScroll();
    });

    var setHeight = function () {
        var height = $(window).height();
        $('.snap').css('height', height * 5);
        $('section.section-fullscreen').css('height', height);
    };

    var snapScroll = function() {
        if( $('#current-media-width').width() == 992 ) {
            $('.snap').snapscroll();
        } else {
            $('.section-fullscreen ').addClass('ss-active');
        }
    };

	// SMOOTH SCROLL
	$('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
	// END: SMOOTH SCROLL

    $(function(){
        init();
    });


    $('form').submit(function(){

        var valid = true;

        $('.error-message').hide();
        $('.submit-error').removeClass('on');
        $('input').removeClass('input-error');
        $('select').removeClass('input-error-select');


        if( ! $('#firstname').val() ) {
            $('#firstname').addClass('input-error');
            valid = false;
        }
        if( ! $('#lastname').val() ) {
            $('#lastname').addClass('input-error');
            valid = false;
        }
        if( ! $('#phonenumber').val() ) {
            $('#phonenumber').addClass('input-error');
            valid = false;
        }
        if( ! $('#email').val() ) {
            $('#email').addClass('input-error');
            valid = false;
        }
        if( ! $('#q1').val() ) {
            $('#q1').addClass('input-error-select');
            valid = false;
        }


        if( ! valid ) {
            $('.submit-error').addClass('on');
            return false;
        }
        else { return true; }

    });


})( jQuery , window , document );